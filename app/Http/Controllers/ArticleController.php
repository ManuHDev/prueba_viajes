<?php

namespace App\Http\Controllers;

use App\Articulo;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        return Articulo::all();
    }

    public function show($id)
    {
        $article = Articulo::find($id);
        return $article;
    }

    public function store(Articulo $request)
    {
        dd($request);
        //$article = Articulo::create($request->all());

        return request; // response()->json($article, 201);
    }

    public function update(Request $request, Articulo $article)
    {
        $article->update($request->all());

        return response()->json($article, 200);
    }

    public function delete(Articulo $article)
    {
        $article->delete();

        return response()->json(null, 204);
    }

}
